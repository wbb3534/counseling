package com.seop.counseling.controller;

import com.seop.counseling.model.CustomerRequest;
import com.seop.counseling.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody CustomerRequest request) {
        customerService.setCustomer(request.getName(), request.getPhone());
        return "전송되었습니다";
    }
}
