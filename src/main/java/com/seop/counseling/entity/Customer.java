package com.seop.counseling.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity //데이터 베이스 테이블이라고 명시하는 명찰
@Getter
@Setter
public class Customer {
    @Id   //pk로 써 사용됨을 표시하는 명찰
    @GeneratedValue(strategy = GenerationType.IDENTITY) //id숫자 1씩 증가 하는 명찰
    private Long id;

    @Column(nullable = false, length = 20)  //컬럼칸에는 무조건 값이 있어야 한다, 글자 갯수는 20개 제한
    private String name;

    @Column(nullable = false, length = 20) //컬럼칸에는 무조건 값이 있어야 한다, 글자 갯수는 20개 제한
    private String phone;
}
