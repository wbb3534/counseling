package com.seop.counseling.repository;

import com.seop.counseling.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> { //데이터 베이스에 접근할수있게 자격을 줘야함  extends JpaRepository<테이블 타입, @ID가 달려있는 메서드에 타입>

}
