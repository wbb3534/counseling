package com.seop.counseling.service;

import com.seop.counseling.entity.Customer;
import com.seop.counseling.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor // 혼자서 일 못한다는 뜻의 명찰
public class CustomerService {
    private final CustomerRepository customerRepository; //컨트롤러에게 들키지 않기 위해 private

    public void setCustomer(String name, String phone) {
        Customer addData = new Customer();
        addData.setName(name);
        addData.setPhone(phone);

        customerRepository.save(addData);    // 레버지토리한테 add 데이터를 저장
    }
}
